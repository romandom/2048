#ifndef PLAYER_H
#define PLAYER_H

class Player{
public:
    // constructors
    Player();

    void generateTile();

private:
    int numberOfTile;
    int x;
    int y;
};

#endif // PLAYER_H
