#include "Game.h"
#include "TileBoard.h"
#include "Button.h"
#include "Tile.h"

#include <QGraphicsTextItem>
#include <QString>
#include <QtDebug>
#include <QPropertyAnimation>
#include <QKeyEvent>

Game::Game(QWidget *parent){
    // set up the screen
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(600,400);

    // set up the scene
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,600,400);
    setScene(scene);
}

void Game::start(){

    if(numberOfBestScore <= numberOfScore)
    {
        numberOfBestScore = numberOfScore;
        newGame = true;
    }

    numberOfScore = 0;
    //clear the screen
    scene->clear();
    //drawing GUI
    drawGUI();

    if(numberOfBestScore > 9)
        bestScore_num->setPos(235, 32);
    if(numberOfBestScore > 99)
        bestScore_num->setPos(230, 32);
    if(numberOfBestScore > 999)
        bestScore_num->setPos(225, 32);
    if(numberOfBestScore > 9999)
    {
        bestScore_num->setHtml("<p1 style='font-size: 12px; font-family:Helvetica Neue;'>"+ QString::number(numberOfBestScore) +"<p>");
        bestScore_num->setPos(222, 33);
    }

    //creating new tileBoard
    tileBoard = new TileBoard();
    tileBoard->placeTiles(140,80,4,4);
    gamePlacedTiles.clear();
    keyEnabled = true;
    createNewTile();
    createNewTile();

}

void Game::drawPanel(int x, int y, int width, int height, QString color, double opacity){
    // draws a panel at the specified location with the specified properties
    QGraphicsRectItem* panel = new QGraphicsRectItem(x,y,width,height);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(color);
    panel->setBrush(brush);
    panel->setOpacity(opacity);
    panel->setPen(Qt::NoPen);
    scene->addItem(panel);
}

void Game::drawGUI()
{
    // draw panel
    drawPanel (160, 70, 290, 290, "#BBADA0",1);
    drawPanel (160, 15, 50, 45, "#BBADA0",1);
    drawPanel (220, 15, 50, 45, "#BBADA0",1);

    //place score text
    QGraphicsTextItem* score = new QGraphicsTextItem("SCORE");
    score->setPos(162, 15);
    score->setDefaultTextColor("#EBE4DA");
    scene->addItem(score);

    //place best score text
    QGraphicsTextItem* best = new QGraphicsTextItem("BEST");
    best->setPos(228, 15);
    best->setDefaultTextColor("#EBE4DA");
    scene->addItem(best);

    //place score
    score_num = new QGraphicsTextItem("");
    score_num->setPos(175, 32);
    score_num->setHtml("<p1 style='font-size: 15px; font-family:Helvetica Neue;'>"+ QString::number(numberOfScore) +"<p>");
    score_num->setDefaultTextColor("#EBE4DA");
    scene->addItem(score_num);

    //place best score
    bestScore_num = new QGraphicsTextItem("");
    bestScore_num->setPos(237, 32);
    bestScore_num->setHtml("<p1 style='font-size: 15px; font-family:Helvetica Neue;'>"+ QString::number(numberOfBestScore) +"<p>");
    bestScore_num->setDefaultTextColor("#EBE4DA");
    scene->addItem(bestScore_num);

    // create the new game button
    Button* newGame = new Button(QString(""));
    newGame->setPos(350,15);
    newGame->setPen(Qt::NoPen);
    newGame->setRect(0,0,100,45);
    connect(newGame,SIGNAL(clicked()),this,SLOT(start()));
    scene->addItem(newGame);

    //place new game text
    QGraphicsTextItem* newGameText = new QGraphicsTextItem("New Game");
    newGameText->setPos(365, 25);
    newGameText->setDefaultTextColor("#F9F6F2");
    scene->addItem(newGameText);
}

void Game::createNewTile()
{
    QString value1;

    int valueOfRendering = (rand() % (1 + 1 - 0) + 0);
    if (valueOfRendering == 0)
    {
        value1 = "2";
    }else
    {
        value1 = "4";
    }
    Tile* tileFirst = new Tile(value1);
    tileFirst->setValueOfTile(value1.toInt());

    tileFirst->setnumberOfTile(newTileValue());

    if (checkIfTileHasSameValue(tileFirst->getnumberOfTile())){
        do{
            tileFirst->setnumberOfTile(newTileValue());
        }while(checkIfTileHasSameValue(tileFirst->getnumberOfTile()));
    }

    QList<Tile *> tiles = tileBoard->getTiles();
    for(size_t i = 0, n = tiles.size(); i < n; i++){
        if (tiles[i]->getnumberOfTile() == tileFirst->getnumberOfTile()){
            tiles[i]->setIsEmpty(false);
            tileFirst->setX(tiles[i]->getX());
            tileFirst->setY(tiles[i]->getY());
            tileFirst->setPos(tiles[i]->getX(), tiles[i]->getY());
            tileFirst->setColor(fillColor(tileFirst->getValueOfTile()));
            tileFirst->setPen(Qt::NoPen);
            gamePlacedTiles.append(tileFirst);
            scene->addItem(tileFirst);
        }
    }
}

void Game::moreMoves()
{
    std::sort(gamePlacedTiles.begin(), gamePlacedTiles.end(), sortAsc);
    QList<Tile *> tiles = tileBoard->getTiles();
    int position_up = 0;
    int position_down = 0;
    int position_right = 0;
    int position_left = 0;
    bool move = false;
    for( auto i : gamePlacedTiles)
    {
        position_up = i->getnumberOfTile();
        position_down = i->getnumberOfTile();
        position_left = i->getnumberOfTile();
        position_right = i->getnumberOfTile();

        if(move)
            break;

        if(position_up != 0 && position_up != 4 && position_up != 8 && position_up != 12)
        {
            while(1)
            {
                position_up -= 1;
                if(tiles[position_up]->getIsEmpty())
                {
                    qDebug() << "There are more moves, empty position";
                    move = true;
                    break;
                }else
                {
                    if(checkIfTileHasSameValue(i->getValueOfTile(), position_up))
                    {
                        qDebug() << "There are more moves, two tiles are same";
                        qDebug() << i->getnumberOfTile() << " " << position_up;
                        move = true;
                        break;
                    }else
                    {
                        break;
                    }
                }
                if(position_up == 0 || position_up == 4 || position_up == 8 || position_up == 12)
                    break;

            }
        }
        if(position_down != 3 && position_down != 7 && position_down != 11 && position_down != 15)
        {
            while(1)
            {
                position_down += 1;
                if(tiles[position_down]->getIsEmpty())
                {
                    qDebug() << "There are more moves, empty position";
                    move = true;
                    break;
                }else
                {
                    if(checkIfTileHasSameValue(i->getValueOfTile(), position_down))
                    {
                        qDebug() << "There are more moves, two tiles are same";
                        qDebug() << i->getnumberOfTile() << " " << position_down;
                        move = true;
                        break;
                    }else
                    {
                        break;
                    }
                }
                if(position_down == 3 || position_down == 7 || position_down == 11 || position_down == 15)
                    break;
            }
        }
        if(position_left != 0 && position_left != 1 && position_left != 2 && position_left != 3)
        {
            while(1)
            {
                position_left -= 4;
                if(tiles[position_left]->getIsEmpty())
                {
                    qDebug() << "There are more moves, empty position";
                    move = true;
                    break;
                }else
                {
                    if(checkIfTileHasSameValue(i->getValueOfTile(), position_left))
                    {
                        qDebug() << "There are more moves, two tiles are same";
                        qDebug() << i->getnumberOfTile() << " " << position_left;
                        move = true;
                        break;
                    }else
                    {
                        break;
                    }
                }
                if(position_left == 0 || position_left == 1 || position_left == 2 || position_left == 3)
                    break;
            }
        }
        if(position_right != 12 && position_right != 13 && position_right != 14 && position_right != 15)
        {
            while(1)
            {
                position_right += 4;
                if(tiles[position_right]->getIsEmpty())
                {
                    qDebug() << "There are more moves, empty position";
                    move = true;
                     break;
                }else
                {
                    if(checkIfTileHasSameValue(i->getValueOfTile(), position_right))
                    {
                        qDebug() << "There are more moves, two tiles are same";
                        qDebug() << i->getnumberOfTile() << " " << position_right;
                        move = true;
                        break;
                    }else
                    {
                        break;
                    }
                }
                if(position_right == 12 || position_right == 13 || position_right == 14 || position_right == 15)
                    break;
            }
        }
    }
    if (!move){
        QGraphicsTextItem* end = new QGraphicsTextItem("");
        end->setPos(70, 367);
        end->setDefaultTextColor("#000000");
        end->setHtml("<p1 style='font-size: 15px; font-family:Helvetica Neue;'> End of the game, give it another chance by pressing button<b> New Game </b><p>");
        scene->addItem(end);
        qDebug() << "No more moves, game should end";
        keyEnabled = false;
    }
}

QString Game::fillColor(int valueOfTile)
{
    switch(valueOfTile)
    {
        case 2:
            return "#EEE4DA";
        case 4:
            return "#EDE0C8";
        case 8:
            return "#F2B179";
        case 16:
            return "#F59563";
        case 32:
            return "#F67C60";
        case 64:
            return "#F65E3B";
        case 128:
            return "#EDCF73";
        case 256:
            return "#EDCC62";
        case 512:
            return "#EDC850";
        case 1024:
            return "#EDC53F";
        case 2048:
            return "#EDC22D";
    }
    return "#000000";
}

void Game::deleteRemovedTiles()
{
    for(auto i : gamePlacedTiles)
        {
            temp.append(i);
        }
    gamePlacedTiles.clear();
    for(auto i : temp){
       if(i->getRemoved())
          {
             continue;
          }else{
             gamePlacedTiles.append(i);
          }
        }
    temp.clear();
}

void Game::keyPressEvent(QKeyEvent * event) //
{
    bool moved = false;
    if (keyEnabled == false)
    {
        return;
    }
    QList<Tile *> tiles = tileBoard->getTiles();
    if (event->key() == Qt::Key_Left)
    {
        std::sort(gamePlacedTiles.begin(), gamePlacedTiles.end(), sortAsc);
        for (auto i : gamePlacedTiles)
        {
            i->setJoined(false);
            int position = i->getnumberOfTile()-4;
            qDebug() << "Position of tile " << position+4 << " with value " << i->getValueOfTile();
            while (1)
            {
                if(position < 0)
                    break;
                if(tiles[position]->getIsEmpty())
                {
                    moved = true;
                    qDebug() << "Tile " << position << " is empty";
                    i->setX(tiles[position]->getX());
                    i->setY(tiles[position]->getY());
                    i->setnumberOfTile(position);
                    tiles[position]->setIsEmpty(false);
                    tiles[position+4]->setIsEmpty(true);
                    i->setFlag(QGraphicsItem::ItemIsFocusable);
                    i->setFocus();
                    i->setPos(tiles[position]->getX(), tiles[position]->getY());
                    scene->update();
                    qDebug() << "Tile " << position << " moved";
                    position -= 4;
                    continue;
                }
                if(!tiles[position]->getIsEmpty()){
                    qDebug() << "Tile " << position << " is not empty";
                    if(checkIfTileHasSameValue(i->getValueOfTile(), position))
                    {
                        moved = true;
                        qDebug() << "Tile " << i->getnumberOfTile() << " has same value as" << position;
                        joinTwoTiles(i->getnumberOfTile(), position);
                        tiles[position+4]->setIsEmpty(true);
                        qDebug() << i->getnumberOfTile();
                        qDebug() << "Join";
                        scene->update();
                    }else{
                        break;
                    }
                }else{
                    break;
                }
                if(position == 0 || position == 1 || position == 2 || position == 3)
                {
                    qDebug() << "break position";
                    break;
                }

            }
        }

        deleteRemovedTiles();
        if(moved)
            createNewTile();
        if(gamePlacedTiles.size() == 16)
            moreMoves();
    }
    if (event->key() == Qt::Key_Right)
    {
        std::sort(gamePlacedTiles.begin(), gamePlacedTiles.end(), sortDesc);
        for (auto i : gamePlacedTiles)
        {
            i->setJoined(false);
            int position = i->getnumberOfTile()+4;
            qDebug() << "Position of tile " << position-4 << " with value " << i->getValueOfTile();
            while (1)
            {
                if(position >15)
                    break;
                if(tiles[position]->getIsEmpty())
                {
                    moved = true;
                    qDebug() << "Tile " << position << " is empty";
                    i->setFlag(QGraphicsItem::ItemIsFocusable);
                    i->setFocus();
                    //move(i, tiles[position]->getX(), tiles[position]->getY());
                    i->setX(tiles[position]->getX());
                    i->setY(tiles[position]->getY());
                    i->setnumberOfTile(position);
                    tiles[position]->setIsEmpty(false);
                    tiles[position-4]->setIsEmpty(true);
                    i->setPos(tiles[position]->getX(), tiles[position]->getY());
                    scene->update();
                    qDebug() << "Tile " << position << " moved";
                    position += 4;
                    continue;
                }
                if(!tiles[position]->getIsEmpty()){
                    qDebug() << "Tile " << position << " is not empty";
                    if(checkIfTileHasSameValue(i->getValueOfTile(), position))
                    {
                        moved = true;
                        qDebug() << "Tile " << i->getnumberOfTile() << " has same value as" << position;
                        joinTwoTiles(i->getnumberOfTile(), position);
                        tiles[position-4]->setIsEmpty(true);
                        qDebug() << i->getnumberOfTile();
                        qDebug() << "Join";
                        scene->update();
                    }else{
                        break;
                    }
                }else{
                    break;
                }
                if(position == 12 || position == 13 || position == 14 || position == 15)
                {
                    qDebug() << "break position";
                    break;
                }
            }
        }
        deleteRemovedTiles();
        if(moved)
            createNewTile();
        if(gamePlacedTiles.size() == 16)
            moreMoves();
    }
    if (event->key() == Qt::Key_Up)
    {
        std::sort(gamePlacedTiles.begin(), gamePlacedTiles.end(), sortAsc);
        for (auto i : gamePlacedTiles)
                {
                    i->setJoined(false);
                    int position = i->getnumberOfTile();
                    if(position == 0 || position == 4 || position == 8 || position == 12)
                    {
                        qDebug() << "break position";
                        continue;
                    }
                    position -= 1;
                    qDebug() << "Position of tile " << position+1 << " with value " << i->getValueOfTile();
                    while (1)
                    {
                        qDebug() << position;
                        if(position < 0)
                            break;
                        if(tiles[position]->getIsEmpty())
                        {
                            moved = true;
                            qDebug() << "Tile " << position << " is empty";
                            i->setX(tiles[position]->getX());
                            i->setY(tiles[position]->getY());
                            i->setnumberOfTile(position);
                            tiles[position]->setIsEmpty(false);
                            tiles[position+1]->setIsEmpty(true);
                            i->setFlag(QGraphicsItem::ItemIsFocusable);
                            i->setFocus();
                            i->setPos(tiles[position]->getX(), tiles[position]->getY());
                            scene->update();
                            qDebug() << "Tile " << position << " moved";
                            if(position == 0 || position == 4 || position == 8 || position == 12)
                            {
                                qDebug() << "break position";
                                break;
                            }
                            position -= 1;
                        }
                        if(!tiles[position]->getIsEmpty()){
                            qDebug() << "Tile " << position << " is not empty";
                            if(checkIfTileHasSameValue(i->getValueOfTile(), position))
                            {
                                moved = true;
                                qDebug() << "Tile " << i->getnumberOfTile() << " has same value as" << position;
                                joinTwoTiles(i->getnumberOfTile(), position);
                                tiles[position+1]->setIsEmpty(true);
                                qDebug() << "Join";
                                scene->update();
                                break;
                            }else
                            {
                                break;
                            }
                        }

                    }
                }
        deleteRemovedTiles();
        if(moved)
            createNewTile();
        if(gamePlacedTiles.size() == 16)
            moreMoves();
    }
    if (event->key() == Qt::Key_Down)
    {
        std::sort(gamePlacedTiles.begin(), gamePlacedTiles.end(), sortDesc);
        for (auto i : gamePlacedTiles)
        {
            i->setJoined(false);
            int position = i->getnumberOfTile();
            if(position == 3 || position == 7 || position == 11 || position == 15)
            {
                qDebug() << "break position 1";
                continue;
            }
            position += 1;
            qDebug() << "Position of tile " << position-1 << " with value " << i->getValueOfTile();
            while (1)
            {
                qDebug() << position;
                if(position < 0 || position > 15)
                    break;
                if(tiles[position]->getIsEmpty())
                {
                    moved = true;
                    qDebug() << "Tile " << position << " is empty";
                    i->setX(tiles[position]->getX());
                    i->setY(tiles[position]->getY());
                    i->setnumberOfTile(position);
                    tiles[position]->setIsEmpty(false);
                    tiles[position-1]->setIsEmpty(true);
                    i->setFlag(QGraphicsItem::ItemIsFocusable);
                    i->setFocus();
                    i->setPos(tiles[position]->getX(), tiles[position]->getY());
                    scene->update();
                    qDebug() << "Tile " << position << " moved";
                    if(position == 3 || position == 7 || position == 11 || position == 15)
                    {
                        qDebug() << "break position";
                        break;
                    }
                    position += 1;
                }
                if(!tiles[position]->getIsEmpty()){
                    qDebug() << "Tile " << position << " is not empty";
                    if(checkIfTileHasSameValue(i->getValueOfTile(), position))
                    {
                        moved = true;
                        qDebug() << "Tile " << i->getnumberOfTile() << " has same value as" << position;
                        joinTwoTiles(i->getnumberOfTile(), position);
                        tiles[position-1]->setIsEmpty(true);
                        qDebug() << "Join " << position;
                        scene->update();
                        break;
                    }else
                    {
                        break;
                    }
                }
            }
        }
        deleteRemovedTiles();
        if(moved)
            createNewTile();
        if(gamePlacedTiles.size() == 16)
            moreMoves();
    }
}

int Game::newTileValue()
{
    return (rand() % (15 + 1 - 0) + 0);
}

int Game::getValueOfTile(int position)
{
    for(auto i : gamePlacedTiles)
    {
        if(i->getnumberOfTile() == position)
            return i->getValueOfTile();
    }
    return 0;
}

bool Game::checkIfTileHasSameValue(int value)
{
    for(auto i : gamePlacedTiles)
    {
        if(i->getnumberOfTile() == value){
            qDebug() << "Values" << i->getnumberOfTile() << " " << value;
            return true;
        }
    }
    return false;
}

bool Game::checkIfTileHasSameValue(int value, int position)
{
    for(auto i : gamePlacedTiles)
    {
        if(i->getValueOfTile() == value && i->getnumberOfTile() == position && !i->getJoined())
        {
            qDebug() << "Values" << i->getValueOfTile() << " " << value;
            return true;
        }
    }
    return false;
}

void Game::joinTwoTiles(int positionA, int positionB)
{
    int sum = 0;
    for(auto i : gamePlacedTiles)
    {
        if(i->getnumberOfTile() == positionA){
            for (auto j : gamePlacedTiles)
            {
                if(j->getnumberOfTile() == positionB){
                    sum = i->getValueOfTile() + j->getValueOfTile();
                    numberOfScore += sum;
                    if(newGame == false || numberOfBestScore < numberOfScore)
                    {
                        numberOfBestScore = numberOfScore;
                    }

                    score_num->setHtml("<p1 style='font-size: 15px; font-family:Helvetica Neue;'>"+ QString::number(numberOfScore) +"<p>");
                    bestScore_num->setHtml("<p1 style='font-size: 15px; font-family:Helvetica Neue;'>"+ QString::number(numberOfBestScore) +"<p>");

                    if(numberOfBestScore > 9)
                        bestScore_num->setPos(235, 32);
                    if(numberOfBestScore > 99)
                        bestScore_num->setPos(230, 32);
                    if(numberOfBestScore > 999)
                        bestScore_num->setPos(225, 32);
                    if(numberOfBestScore > 9999)
                    {
                        bestScore_num->setHtml("<p1 style='font-size: 12px; font-family:Helvetica Neue;'>"+ QString::number(numberOfBestScore) +"<p>");
                        bestScore_num->setPos(222, 33);
                    }

                    if(numberOfScore > 9)
                        score_num->setPos(175, 32);
                    if(numberOfScore > 99)
                        score_num->setPos(170, 32);
                    if(numberOfScore > 999)
                        score_num->setPos(165, 32);
                    if(numberOfScore > 9999)
                    {
                        score_num->setHtml("<p1 style='font-size: 12px; font-family:Helvetica Neue;'>"+ QString::number(numberOfScore) +"<p>");
                        score_num->setPos(162, 33);
                    }

                    i->setRemoved(true);
                    i->setnumberOfTile(-1);
                    j->setValueOfTile(sum);
                    j->setColor(fillColor(sum));
                    j->setJoined(true);
                    scene->removeItem(i);
                    scene->update();
                    qDebug() << "Joining " << i->getnumberOfTile() << " " << j->getnumberOfTile();
                    break;
                }
            }
            break;
        }
    }
}

bool Game::sortAsc(Tile *v1, Tile  *v2)
{
    return v1->getnumberOfTile() < v2->getnumberOfTile();
}

bool Game::sortDesc(Tile *v1, Tile *v2)
{
    return v1->getnumberOfTile() > v2->getnumberOfTile();
}



void Game::displayMainMenu()
{
    //create the title text
    QGraphicsTextItem* titleText = new QGraphicsTextItem(QString("2048"));
    QFont titleFont("Helvetica Neue", 50);
    titleText->setFont(titleFont);
    int txPos = this->width()/2 - titleText->boundingRect().width()/2;
    int tyPos = 60;
    titleText->setPos(txPos, tyPos);
    scene->addItem(titleText);

    // create the play button
    Button* playButton = new Button(QString("Play"));
    int bxPos = this->width()/2 - playButton->boundingRect().width()/2;
    int byPos = 200;
    playButton->setPos(bxPos,byPos);
    playButton->setPen(Qt::NoPen);
    connect(playButton,SIGNAL(clicked()),this,SLOT(start()));
    scene->addItem(playButton);

    // create the quit button
    Button* quitButton = new Button(QString("Quit"));
    int qxPos = this->width()/2 - quitButton->boundingRect().width()/2;
    int qyPos = 280;
    quitButton->setPos(qxPos,qyPos);
    quitButton->setPen(Qt::NoPen);
    connect(quitButton,SIGNAL(clicked()),this,SLOT(close()));
    scene->addItem(quitButton);
}
