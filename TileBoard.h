#ifndef TILEBOARD_H
#define TILEBOARD_H

#include <QList>
#include "Tile.h"

class TileBoard{
public:
    //constructors
    TileBoard();

    //getters/setters
    QList<Tile*> getTiles();

    //public methods
    void placeTiles(int x, int y, int cols, int rows);
private:
    void createTileColumn(int x, int y, int numOfRows, size_t i);
    QList<Tile*> tiles;
};

#endif // TILEBOARD_H
