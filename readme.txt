Hlavné menu obsahuje názov hry a 2 možnosti:
-možnosť začať hru "Play"
-možnosť ukončiť aplikáciu "Quit"

Ovládanie hry:
Po stlačení možnosti "Play" sa hra ovláda šipkami na klávesnici.
Šipka doprava - dlaždice sa posunú doprava
Šipka doľava - dlaždice sa posunú doľava
Šipka hore - dlaždice sa posunú hore
Šipka dole - dlaždice sa posunú dole

V hre je taktiež vbudované vypočítavanie skore a najlepšieho skore.

Herná logika v jednoduchosti spočíva v tom, že po stlačení tlačidla sa preiterujú dlaždice v liste. Každá dlaždica sa dostane do cyklu
while, ktorý je nekonečný a ukončí sa až keď má pri sebe dlaždica kraj hracej dosky, alebo dlaždicu inej hodnoty. Ak sa tam nachádza dlaždica
rovnakej hodnoty, dlaždice sa spoja a prepíše sa tá dlaždica, ktorá je v smere spájania a tá druhá sa označí ako removed. Na konci iterácie sa 
dlaždice, ktoré sú označené ako removed vymažu a vytvorí sa nová dlaždica. Ak je doska plna dlaždíc, skontroluje sa, či je možný ďalší pohyb.
Ak nie je, vypíše sa fráza pod dosku s dlaždicami. 

Popis funkcii v triede Game:
void createNewTile() - vytvára novú hernú dlaždicu v hre
void deleteRemovedTiles() - vymazáva z listu dlaždice, ktoré bolo označené ako removed
void drawGUI() - vykresľuje hernú ponuku
void drawPanel(int x, int y, int width, int height, QString color, double opacity) - vykresľuje herný panel aj s textami v hre
void joinTwoTiles(int positionA, int positionB) - spája 2 dlaždice rovnakej hodnoty
void moreMoves() - zisťuje, či je možné vykonať v hre nejaký ďalší krok
bool checkIfTileHasSameValue(int value) - zisťuje, či dlaždice majú rovnakú hodnotu
bool checkIfTileHasSameValue(int value, int position) - zisťuje, či dlaždice majú rovnakú hodnotu
QString fillColor(int valueOfTile) - funkcia vracia v stringu farbu pre danu dlaždicu
int getValueOfTile(int position) - zisťuje podľa pozície hodnotu dlaždice
int newTileValue() - generuje novú hodnotu dlaždice
static bool sortAsc(Tile  *v1, Tile  *v2) - zoraďuje dlaždice vzostupne
static bool sortDesc(Tile *v1, Tile *v2) - zoraďuje dlaždice zostupne

Popis funkcii v triede Button:
void mousePressEvent(QGraphicsSceneMouseEvent *event) - pri prejdeni kurzorom je vyslaný signál
void hoverEnterEvent(QGraphicsSceneHoverEvent *event) - pri prejdeni kurzorom sa tlačidlo zosvetli
void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) - pri opustení tlačidla kurzorom sa tlačidlo vráti na pôvodnú farbu

V triede Tile sú gettre a settre, ktoré nastavujú alebo vracajú hodnoty atribútov v objekte.

Popis funkcii v triede TileBoard:
void placeTiles(int x, int y, int cols, int rows) - vytvára hernu dosku z funkcie createTileColumn
void createTileColumn(int x, int y, int numOfRows, size_t i) - vytvára stlpec dlaždic
