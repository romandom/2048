#ifndef GAME_H
#define GAME_H

#include "Tile.h"
#include "TileBoard.h"

#include <QtAlgorithms>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QList>
#include <QString>


class Game: public QGraphicsView{
    Q_OBJECT
public:
    //Constructors
    Game(QWidget* parent=NULL);

    //public methods
    void displayMainMenu();
    void keyPressEvent(QKeyEvent * event);

    //public attributes
    QGraphicsScene* scene;
    TileBoard* tileBoard;

public slots:
    void start();

private:
    void createNewTile();
    void deleteRemovedTiles();
    void drawGUI();
    void drawPanel(int x, int y, int width, int height, QString color, double opacity);
    void joinTwoTiles(int positionA, int positionB);
    void moreMoves();
    bool checkIfTileHasSameValue(int value);
    bool checkIfTileHasSameValue(int value, int position);
    QString fillColor(int valueOfTile);
    int getValueOfTile(int position);
    int newTileValue();
    static bool sortAsc(Tile  *v1, Tile  *v2);
    static bool sortDesc(Tile *v1, Tile *v2);

    //Attributes
    QGraphicsTextItem* bestScore_num;
    QGraphicsTextItem* score_num;
    QList<Tile*> gamePlacedTiles;
    QList<Tile*> temp;
    bool keyEnabled = false;
    bool newGame = false;
    int numberOfScore = 0;
    int numberOfBestScore = 0;

};

#endif // GAME_H
