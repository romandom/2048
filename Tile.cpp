#include "Tile.h"
#include "Game.h"
#include <QPointF>
#include <QPolygonF>
#include <QVector>
#include <QBrush>

Tile::Tile(QGraphicsItem *parent){
    // draw the polygon

    // points needed to draw hexagon: (1,0), (2,0), (3,1), (2,2), (1,2), (0,1)
    QVector<QPointF> tilePoints;
    tilePoints << QPointF(1,0) << QPointF(3,0) << QPointF(3,2) << QPointF(1,2)
              << QPointF(1,0);

    // scale the points
    int SCALE_BY = 30;
    for (size_t i = 0, n = tilePoints.size(); i < n; ++i){
        tilePoints[i] = tilePoints[i] * SCALE_BY;
    }

    // create a polygon with the scaled points
    QPolygonF hexagon(tilePoints);

    // draw the poly
    setPolygon(hexagon);

}

Tile::Tile(QString value, QGraphicsItem *parent)
{
    // draw the polygon

    // points needed to draw hexagon: (1,0), (2,0), (3,1), (2,2), (1,2), (0,1)
    QVector<QPointF> tilePoints;
    tilePoints << QPointF(1,0) << QPointF(3,0) << QPointF(3,2) << QPointF(1,2)
              << QPointF(1,0);

    // scale the points
    int SCALE_BY = 30;
    for (size_t i = 0, n = tilePoints.size(); i < n; ++i){
        tilePoints[i] = tilePoints[i] * SCALE_BY;
    }

    // create a polygon with the scaled points
    QPolygonF hexagon(tilePoints);

    // draw the poly
    setPolygon(hexagon);

    text = new QGraphicsTextItem(value,this);
    int xPos = 48;
    int yPos = 12;
    text->setPos(xPos,yPos);
    //  qDebug() << "farba " << value;
    if(value == "2" || value == "4")
    {
        text->setDefaultTextColor("#776E65");
    }else{
        text->setDefaultTextColor("#F9F6F2");
    }
    text->setHtml("<p1 style='font-size: 30px; font-family:Helvetica Neue;'>"+ value +"<p>");
}

void Tile::setnumberOfTile(int number){
    numberOfTile = number;
}

void Tile::setX(int x){
    this->x = x;
}

void Tile::setY(int y){
    this->y = y;
}

void Tile::setColor(QString color)
{
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(color);
    setBrush(brush);
}

void Tile::setValueOfTile(int value)
{
    text->setHtml("<p1 style='font-size: 30px; font-family:Helvetica Neue;'>"+ QString::number(value) +"<p>");
    if(value == 2 || value == 4)
    {
        text->setDefaultTextColor("#776E65");
    }else{
        text->setDefaultTextColor("#F9F6F2");
        if(value > 9 && value < 100)
            text->setPos(40, 12);
        if(value > 100 && value < 1000)
            text->setPos(30, 12);
        if(value > 1000 && value < 10000)
        {
            text->setHtml("<p1 style='font-size: 20px; font-family:Helvetica Neue;'>"+ QString::number(value) +"<p>");
            text->setPos(34, 16);
        }
    }

    valueOfTile = value;
}

void Tile::setIsEmpty(bool value)
{
    empty = value;
}

void Tile::setJoined(bool value)
{
    joined = value;
}

void Tile::setRemoved(bool value)
{
    removed = value;
}

int Tile::getX(){
    return x;
}

int Tile::getY(){
    return y;
}

int Tile::getnumberOfTile()
{
    return numberOfTile;
}

int Tile::getValueOfTile()
{
    return valueOfTile;
}

bool Tile::getIsEmpty()
{
    return empty;
}

bool Tile::getJoined()
{
    return joined;
}

bool Tile::getRemoved()
{
    return removed;
}

