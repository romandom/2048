#include "TileBoard.h"
#include "Game.h"



extern Game* game;

TileBoard::TileBoard()
{

}

QList<Tile *> TileBoard::getTiles(){
    return tiles;
}

void TileBoard::placeTiles(int x, int y, int cols, int rows)
{
    int X_SHIFT = 70;

    for (size_t i = 0, n = cols; i < n; i++){
        createTileColumn(x+X_SHIFT*i,y,rows, i);
    }
}

void TileBoard::createTileColumn(int x, int y, int numOfRows, size_t row)
{
    //creates a column of Tiles at the specified location with the specified number of rows
    for (size_t i = 0, n = numOfRows; i < n; i++){
        int numberOfTile = i + (int(row) * 4);
        Tile* tile = new Tile();
        tile->setPos(x,y+70*i);
        tile->setX(x);
        tile->setY(y+70*i);
        tile->setnumberOfTile(numberOfTile);
        tile->setColor("#CDC1B4");
        tile->setPen(Qt::NoPen);
        tiles.append(tile);
        game->scene->addItem(tile);
    }
}
