#ifndef TILE_H
#define TILE_H

#include <QGraphicsPolygonItem>
#include <QGraphicsRectItem>

class Tile: public QGraphicsPolygonItem{
public:
    // constructors
    Tile(QGraphicsItem* parent=NULL);
    Tile(QString name, QGraphicsItem* parent=NULL);

    // getters
    int getX();
    int getY();
    int getnumberOfTile();
    int getValueOfTile();
    bool getIsEmpty();
    bool getJoined();
    bool getRemoved();

    // setters
    void setnumberOfTile(int number);
    void setX(int x);
    void setY(int y);
    void setColor(QString color);
    void setValueOfTile(int value);
    void setIsEmpty(bool value);
    void setJoined(bool value);
    void setRemoved(bool value);

private:
    bool joined = false;
    int numberOfTile;
    int x;
    int y;
    int valueOfTile;
    bool empty = true;
    bool removed = false;
    QGraphicsTextItem* text;
};

#endif // TILE_H
